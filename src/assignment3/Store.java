package assignment3;

import assginment2.Inventory;
import assignment1.Game;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

public class Store {

    private Inventory inventory;

    public static final String INVENTORY_URL = "https://gist.githubusercontent.com/tacksoo/b1d2da649f14840b598f6ce12ee2f68a/raw/d56aac3c0d2e6f70aa646a9a7077b901ba4e1bed/inventory.csv";

    public Store(String urlString) {
        loadInventoryFromWeb(urlString);
    }

    public void loadInventoryFromWeb(String inventoryUrl) {
        inventory = new Inventory();
        try {
            String csvString = IOUtils.toString(new URL(inventoryUrl),"UTF-8");
            CSVParser parser = new CSVParser(new StringReader(csvString),
                    CSVFormat.DEFAULT.builder().setHeader().setIgnoreSurroundingSpaces(true).build());
            for(CSVRecord record: parser) {
                String steamid = record.get("id");
                String name = record.get("game name");
                Game g = new Game(steamid);
                g.getGameInfoFromSteam();
                inventory.add(g);
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public Inventory getInventory() {
        return inventory;
    }
}
