package assginment2;

import assignment1.Game;

import java.util.ArrayList;

public class Inventory {

    private ArrayList<Game> games;

    public Inventory() {
        games = new ArrayList<>();
    }

    /**
     * Check for duplicate and only add if a duplicate does not exist
     * @param g
     */
    public void add(Game g) {
        for (int i = 0; i < games.size(); i++) {
            if (games.get(i).getSteamID().equals(g.getSteamID())) {
                return;
            }
        }
        games.add(g);
    }

    public void remove(Game g) {
        for (int i = 0; i < games.size(); i++) {
            if (games.get(i).getSteamID().equals(g.getSteamID())) {
                games.remove(i);
            }
        }
    }

    public Game findMostExpensiveGame() {
        if (games.size() == 0) return null;

        Game mostExpensive = games.get(0);
        for (int i = 1; i < games.size(); i++) {
            if (games.get(i).getPrice() > mostExpensive.getPrice()) {
                mostExpensive = games.get(i);
            }
        }
        return mostExpensive;
    }

    public Game findCheapestGame() {
        if (games.size() == 0) return null;

        Game cheapest = games.get(0);
        for (int i = 1; i < games.size(); i++) {
            if (games.get(i).getPrice() < cheapest.getPrice()) {
                cheapest = games.get(i);
            }
        }
        return cheapest;
    }




    public int getSize() {
        return games.size();
    }

    public Game findMostHighlyRatedGame() {
        if (games.size() == 0 ) return null;

        Game best = games.get(0);
        for (int i = 0; i < games.size(); i++) {
            if(Integer.parseInt(games.get(i).getRating()) > Integer.parseInt(best.getRating())) {
                best = games.get(i);
            }
        }
        return best;
    }


    public void printAveragePriceOfAllGames() {
        double total = 0;
        for (int i = 0; i < games.size(); i++) {
            total += games.get(i).getPrice();
        }
        double average =  total/games.size();
        System.out.println(average);
    }

    public double getAveragePriceOfAllGames() {
        double total = 0;
        for (int i = 0; i < games.size(); i++) {
            total += games.get(i).getPrice();
        }
        double average =  total/games.size();
        return average;
    }

}
