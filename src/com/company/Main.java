package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        System.out.println("Hello World!");
    }

    public static String greetPerson(String name) {
        return "Hello " + name + "!";
    }

    // leap year
    public static boolean isLeapyear(int year) {
        if (year % 400 == 0) return true;
        if (year % 100 == 0) return false;
        if (year % 4 == 0)
            return true;
        else
            return false;
    }

}
