import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.net.URL;
import java.util.List;

public class MidtermTest {

    private static String CSV_DATES = "https://gist.githubusercontent.com/tacksoo/d1fcb51f8921cdc90d1ffadb0b63b768/raw/6c9a8b9ffadd87b4bd0217b91cdd90bb9e227ef2/schedule.csv";

    private static String CSV_STONKS = "https://gist.githubusercontent.com/tacksoo/b9edbfc8c03e1ca89d459bf1af39842d/raw/75abf553a0297d9202b1a568f185f735055d6f81/stonks.csv";

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("web.driver.geckodriver","geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void testHackerNews() {
        driver.get("https://news.ycombinator.com");
        List<WebElement> spans = driver.findElements(By.className("rank"));
        Assert.assertEquals(30, spans.size());
    }

    @Test
    public void testLineCount() throws Exception {
        String str = IOUtils.toString(new URL(CSV_DATES), "UTF-8");
        String[] lines = str.split("\n");
        Assert.assertEquals(29, lines.length);
    }

    @Test
    public void testStonkLowest() throws Exception {
        String str = IOUtils.toString(new URL(CSV_STONKS), "UTF-8");
        String[] lines = str.split("\n");
        String lowest = "";
        double lowestPercent = Double.MAX_VALUE;
        for (int i = 1; i < lines.length; i++) {
            String[] cols = lines[i].split(",");
            String percent = cols[2];
            percent = percent.replace("%","");
            if (Double.parseDouble(percent) < lowestPercent) {
                lowest = lines[i];
                lowestPercent = Double.parseDouble(percent);
            }
        }
        FileUtils.writeStringToFile(new File("stonks.csv"),lowest,"UTF-8");
    }


}
