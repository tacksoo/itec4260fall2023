import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.nio.channels.SelectableChannel;

public class TuitionCalculatorTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        driver = new FirefoxDriver();
    }

    @Test
    public void testTuitionCalculator() {
        driver.get("https://www.ggc.edu/admissions/tuition-and-financial-aid-calculators");
        WebElement selectElement = driver.findElement(By.cssSelector("#creditHOURS"));
        Select creditHours = new Select(selectElement);
        creditHours.selectByVisibleText("9 hours");


    }

    @AfterClass
    public static void cleanUp() {
        //driver.close();
    }

}
