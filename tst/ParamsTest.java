import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.converters.Param;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(JUnitParamsRunner.class)
public class ParamsTest {

    @Test
    @Parameters({"1,2,3","2,3,4","4,5,6"})
    public void testAddition(int a, int b, int c) {
        Assert.assertEquals(a+b+c, a+b+c);
    }

    @Test
    @Parameters({"lisa,sam,jules","a,b,c","tom,jerry,thom"})
    public void testNames(String a, String b, String c) {
        Assert.assertNotNull(a+b+c);
    }

}
