import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.Date;
import java.sql.*;
import java.time.Duration;
import java.util.List;

public class CarListingTest {

    private static WebDriver driver;
    private static Connection connection;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver", "geckodriver");
        driver = new FirefoxDriver();
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:cars.sqlite");
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public int getTableCount(String tableName) {
        String sql = "select count(*) from " + tableName;
        int count = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            rs.next();
            count = rs.getInt("count(*)");
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return count;
    }


    @Test
    public void testCraigsListCars() {
        int beforeCount = getTableCount("cars");
        driver.get("https://atlanta.craigslist.org/d/cars-trucks/search/cta");
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        WebElement gallery = driver.findElement(By.id("search-results-page-1"));
        List<WebElement> cars = gallery.findElements(By.tagName("li"));
        for (WebElement car: cars) {
            WebElement label = findElement(car, "label");
            WebElement priceinfo = findElement(car, "priceinfo");
            WebElement carLink = car.findElement(By.tagName("a"));
            String title = "";
            int price = -1;
            String url = "";
            Date d = new Date();
            String currentDate = (new Date()).toString();
            if (label != null)
              title = label.getText();
            else
              title = "Mystery car";
            if (priceinfo != null)
                price = Integer.parseInt(priceinfo.getText().replace("$","").replace(",",""));
            else
                price = -1;
            if (carLink.getAttribute("href") != "") {
                url = carLink.getAttribute("href");
            } else {
                url = "";
            }
            addCarListingtoDatabase(title, price, url, currentDate);
        }
        int afterCount = getTableCount("cars");
        Assert.assertEquals(120, cars.size());
        Assert.assertEquals("table should have 120 more rows",beforeCount+120,afterCount);
    }

    private void addCarListingtoDatabase(String title, int price, String url, String currentDate) {
        String sql = "insert into cars (subject, price, url, timestamp) values (?, ?, ? , ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, title);
            ps.setInt(2, price);
            ps.setString(3, url);
            ps.setString(4, currentDate);
            ps.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public WebElement findElement(WebElement element, String className) {
        try {
            return element.findElement(By.className(className));
        } catch(NoSuchElementException e) {
            return null;
        }
    }


    @AfterClass
    public static void cleanUp() throws SQLException {
        driver.close();
        connection.close();
    }
}
