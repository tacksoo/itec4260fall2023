import assginment2.Inventory;
import assignment3.Store;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.URL;

/**
 * Steam API prices are highly reliable
 * Please be careful!
 *
 */
public class StoreTest {

    private Store store;

    @Before
    public void setUp() {
        store = new Store(Store.INVENTORY_URL);
    }

    @Test
    public void testCheapestGame() {
        Inventory inv = store.getInventory();
        // cheapest game is "portal 2" in the inventory csv file
        Assert.assertEquals(99,inv.findCheapestGame().getPrice(),0);
    }

    @Test
    public void testMostExpensiveGame() {
        Inventory inv = store.getInventory();
        // most expensive is the first game with the highest price
        // most expensive is "rust" in the inventory
        Assert.assertEquals(3999,inv.findMostExpensiveGame().getPrice(),0);
    }

    @Test
    public void testAveragePriceOfAllGame() {
        Inventory inv = store.getInventory();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        inv.printAveragePriceOfAllGames();
        // the total price of the four games in the inventory is
        double average =  (3999 + 3999 + 99 + 1999)/4;
        Assert.assertEquals(average, Double.parseDouble(baos.toString()), 10);
    }


    @Test
    public void testDownloadDefaultURL() throws Exception {
       Store mystore = new Store(Store.INVENTORY_URL);
       Inventory inventory = mystore.getInventory();
        Assert.assertEquals(4, inventory.getSize());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDownloadExternalURL() {
        Store mystore = new Store("https://www.google.com");
    }
}
