import assginment2.Inventory;
import assignment1.Game;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class InventoryTest {

    private static Inventory games = new Inventory();
    // resident evil 2 remake, elden ring, fallout 4,
    private static String[] steamIDs= { "883710", "1245620", "377160" };

    @Before
    public void setUp() {
        for (int i = 0; i < steamIDs.length; i++) {
            Game g = new Game(steamIDs[i]);
            g.getGameInfoFromSteam();
            //g.printGame();
            games.add(g);
        }
    }

    @Test
    public void testAddingToInventory() {
        int size = games.getSize();
        Game g = new Game("894020"); // the name of the game is Death Door
        g.getGameInfoFromSteam();
        games.add(g);
        Assert.assertEquals("When you add one game, the inventory increases by one",size+1,games.getSize());
        // let's test adding a duplicate game
        Game d = new Game("883710");
        g.getGameInfoFromSteam();
        games.add(d);
        Assert.assertEquals("Duplicate game added",size+1,games.getSize());
    }

    @Test
    public void testRemoveGameFromInventory() {
        int size = games.getSize();
        Game g = new Game("883710");
        g.getGameInfoFromSteam();
        games.remove(g);
        Assert.assertEquals(size-1, games.getSize());
    }

    @Test
    public void testCheapestGame() {
        // cheapest in the inventory is fallout 4, 1999 cents
        Game cheapest = games.findCheapestGame();
        Assert.assertEquals(1999.0,cheapest.getPrice(),0);
    }

    @Test
    public void testMostHighlyRated() {
        Game highlyrated = games.findMostHighlyRatedGame();
        Assert.assertEquals("1245620",highlyrated.getSteamID());
    }

    @Test
    public void testPrintAveragePriceOfAllGames() {
        // from: https://stackoverflow.com/questions/8708342/redirect-console-output-to-string-in-java
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);

        games.printAveragePriceOfAllGames();

        Assert.assertEquals("fails when games are on sale",(1999+3999+5999)/3,Double.parseDouble(baos.toString()),10);

    }
}
