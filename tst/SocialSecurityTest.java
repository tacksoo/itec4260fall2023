import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

@RunWith(JUnitParamsRunner.class)
public class SocialSecurityTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        driver = new FirefoxDriver();
    }

    @Test
    @Parameters({"1,1,2001,168600","1,1,1990,300000","1,1,1980,400000","1,1,1970,500000","1,1,2000,10000000"})
    public void testBenefits(String m, String d, String y, String salary) throws IOException {
        driver.get("https://www.ssa.gov/OACT/quickcalc/");
        System.out.println(driver.getTitle());
        WebElement month = driver.findElement(By.id("month"));
        WebElement day = driver.findElement(By.id("day"));
        WebElement year = driver.findElement(By.id("year"));
        WebElement earnings = driver.findElement(By.id("earnings"));
        month.clear();
        day.clear();
        year.clear();
        earnings.clear();
        month.sendKeys(m);
        day.sendKeys(d);
        year.sendKeys(y);
        earnings.sendKeys(salary);
        WebElement submit = driver.findElement(By.cssSelector("body > table:nth-child(6) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > form:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(1) > input:nth-child(1)"));
        submit.submit();
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#est_fra")));
        WebElement benefitAmount = driver.findElement(By.cssSelector("#est_fra"));
        double benefit = Double.parseDouble(benefitAmount.getText().replace("$","").replace(",",""));
        String record = m + "," + d + "," + y + "," + salary + "," + benefit;
        FileUtils.writeStringToFile(new File("benefits.csv"),record + "\n","UTF-8",true);
    }

    @Test
    @Parameters({"0","5","10","15","20"})
    public void testTenYearGap(int gapYears) throws IOException {
        driver.get("https://www.ssa.gov/OACT/quickcalc/");
        System.out.println(driver.getTitle());
        WebElement month = driver.findElement(By.id("month"));
        WebElement day = driver.findElement(By.id("day"));
        WebElement year = driver.findElement(By.id("year"));
        WebElement earnings = driver.findElement(By.id("earnings"));
        month.clear();
        day.clear();
        year.clear();
        earnings.clear();
        month.sendKeys("1");
        day.sendKeys("1");
        year.sendKeys("1970");
        earnings.sendKeys("60000");
        WebElement submit = driver.findElement(By.cssSelector("body > table:nth-child(6) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > form:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(1) > input:nth-child(1)"));
        submit.submit();
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#est_fra")));
        // go to the earnings used page
        WebElement button = driver.findElement(By.cssSelector("body > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(1) > form:nth-child(2) > input:nth-child(10)"));
        button.submit();


        WebDriverWait wait2 = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait2.until(ExpectedConditions.presenceOfElementLocated(By.name("2023")));
        // 0 gap years mean, starts work at age 22
        int adultYear = 1970 + 17;
        int startYear = 1970 + 22 + gapYears;
        for (int i = adultYear; i < startYear; i++) {
            WebElement e = driver.findElement(By.name(String.valueOf(i)));
            e.clear();
            e.sendKeys("0");
        }

        for (int i = startYear; i <= 2023; i++) {
            WebElement e = driver.findElement(By.name(String.valueOf(i)));
            e.clear();
            e.sendKeys("60000");
        }

        WebElement reclacButton = driver.findElement(By.cssSelector("body > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > form:nth-child(3) > input:nth-child(7)"));
        reclacButton.submit();


        WebDriverWait wait3 = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait3.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#est_fra")));
        WebElement benefitAmount = driver.findElement(By.cssSelector("#est_fra"));
        double benefit = Double.parseDouble(benefitAmount.getText().replace("$","").replace(",",""));
        FileUtils.writeStringToFile(new File("gaps.csv"),gapYears + ";" + benefit + "\n","UTF-8",true);
    }


    @AfterClass
    public static void cleanUp() {
        //driver.close();
    }

}
