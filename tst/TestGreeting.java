import com.company.Main;
import org.junit.Assert;
import org.junit.Test;


import java.util.Random;

public class TestGreeting {


    // junit:junit:4.13.2
    @Test
    public void testGreet() {
        // input value: Juan
        // expect value: "Hello Juan!"
        // actual value: ??? (have to call the actual method)
        Assert.assertEquals("Hello Juan!", Main.greetPerson("Juan"));
        Assert.assertEquals("Hello Lisa!", Main.greetPerson("Lisa"));
        Assert.assertEquals("Hello 철수!", Main.greetPerson("철수"));
    }

    @Test
    public void testRandomNum() {
        Random r = new Random();
        System.out.println(r.nextInt(6));

    }


}
