import assginment2.Inventory;
import assginment2.InventoryInterface;
import assignment1.Game;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class InventoryMockTest {

    @Test
    public void testCheapestGameMock() {
        Inventory invMock = Mockito.mock(Inventory.class);
        Mockito.when(invMock.findCheapestGame()).thenReturn(
                new Game("12345","PC","Madden NFL","01/01/2023","EA Games","Sports",60,"5.0"));
        Assert.assertEquals("Madden NFL", invMock.findCheapestGame().getName());
    }

    @Test
    public void testMostExpensiveGameMock() {
        Inventory invMock = Mockito.mock(Inventory.class);
        Mockito.when(invMock.findMostExpensiveGame()).thenReturn(
                new Game("23456","PC","Super Mario Bros", "11/11/2023", "Nintendo","Platform",60,"5.0"));
        Assert.assertEquals("Super Mario Bros", invMock.findMostExpensiveGame().getName());
    }

    @Test
    public void testAveragePriceGameMock() {
        Inventory invMock = Mockito.mock(Inventory.class);
        Mockito.when(invMock.getAveragePriceOfAllGames()).thenReturn(60.0);
        Assert.assertEquals(60.0, invMock.getAveragePriceOfAllGames(),0);
    }
}
